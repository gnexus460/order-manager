<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'OrderController@create')->name('order.create');
Route::post('/', 'OrderController@store')->name('order.store');
Route::get('/show', 'OrderController@index')->name('order.show');
Route::post('/search', 'OrderController@search')->name('order.search');