@extends('welcome')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Отчет о заказах</h1>
                <form action="{{ route('order.show') }}" method="GET" style="margin-bottom: 50px">
                    @csrf
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">№ заказа</label>
                                <input name="id" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            {{--{{ dd(Cookie::get()) }}--}}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Город</label>
                                <input name="city" value="" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Сумма заказа</label>
                                <select class="form-control" name="max-amount" id="exampleFormControlSelect1">
                                    <option></option>
                                    <option>До 100 гривен</option>
                                    <option>До 500 гривен</option>
                                    <option>До 1000 гривен</option>
                                    <option>До 5000 гривен</option>
                                    <option>До 10000 гривен</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Найти" />

                </form>
                <div class="result">
                    <div class="result-title">
                        <h2>Найдено {{ $orders->count() }} заказов</h2>
                        <p>На сумму {{ $orders->sum('amount') }} гривен или ${{  App\Order::convertUAHtoUSD($orders->sum('amount')) }}</p>
                    </div>
                    <div class="result-table">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">№ заказа</th>
                                <th scope="col">Имя</th>
                                <th scope="col">Фамилия</th>
                                <th scope="col">Телефон</th>
                                <th scope="col">Эл.Почта</th>
                                <th scope="col">Город</th>
                                <th scope="col">Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr>
                                    <th scope="row">{{ $order->id }}</th>
                                    <td>{{ $order->name }}</td>
                                    <td>{{ $order->last_name }}</td>
                                    <td>{{ $order->phone }}</td>
                                    <td>{{ $order->email }}</td>
                                    <td>{{ $order->city }}</td>
                                    <td>{{ $order->amount }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $orders->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection