@extends('welcome')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-5 offset-md-3">
                <h1>Создать заказ</h1>
                <form action="{{ route('order.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Имя клиента</label>
                        <input name="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Фамилия клиента</label>
                        <input name="last_name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Номер телефона</label>
                        <input name="phone" id="phone_selector"  type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="+38">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Эл.почта</label>
                        <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Город</label>
                        <input name="city" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Сумма</label>
                        <input name="amount" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="00.00">
                        <small id="emailHelp" class="form-text text-muted">Гривен</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Создать</button>
                    <a href="{{ route('order.create') }}" type="button" id="clear" class="btn btn-secondary">Очистить</a>
                </form>
            </div>
        </div>
    </div>
@endsection