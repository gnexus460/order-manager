import 'bootstrap';
import IMask from 'imask';

var element = document.getElementById('phone_selector');
if(element) {
    var maskOptions = {
        mask: '+{38}(000)000-00-00'
    };
    var mask = new IMask(element, maskOptions);
}


window.addEventListener('DOMContentLoaded', () => {
    const clear = document.getElementById('clear');
    if(clear) {
        clear.addEventListener('click', (e) => {
            e.preventDefault();
            let inputs = document.getElementsByClassName('form-control');
            for(let el of inputs) {
                el.value = '';
            }
        });
    }
});
