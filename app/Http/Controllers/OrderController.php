<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();


        $orders =  Order::select()
            ->when(!empty($data['max-amount']), function ($query) use ($data) {
                $max_amount = preg_replace('~[^0-9]+~','',$data['max-amount']);
                return $query->where('amount','<=', $max_amount);
            })
            ->when(!empty($data['id']), function ($query) use ($data) {
                return $query->where('id', $data['id']);
            })
            ->when(!empty($data['city']), function ($query) use ($data) {

                return $query->where('city', $data['city']);
            })
            ->orderBy('created_at', 'desc')
            ->paginate(10)->appends(!empty($data['city']) ? ['city' => $data['city']]:'');

        return view('pages.order-list')->withOrders($orders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.create-order');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name'         => 'required|string|min:3|max:150',
            'last_name'    => 'required|string|min:3|max:150',
            'phone'        => 'required|min:17|max:17',
            'email'        => 'required|string|email|max:255',
            'city'         => 'required|string|min:3|max:150',
            'amount'       => 'required|numeric|between:0,999999.99',
        ));

        $order = new Order;
        $order->name = $request->name;
        $order->last_name = $request->last_name;
        $order->phone = preg_replace('~[^0-9]+~','',$request->phone);
        $order->email = $request->email;
        $order->city = $request->city;
        $order->amount = $request->amount;
        $order->save();

        Session::flash('success','Заказ был создан успешно');
        return redirect()->route('order.create');
    }
}
