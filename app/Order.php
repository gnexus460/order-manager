<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public static function convertUAHtoUSD($amount)
    {
        $url = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';

        $context = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'timeout' => 60
            )
        ));

        $resp = json_decode(file_get_contents($url, FALSE, $context));
        foreach($resp as $currency) {
            if($currency->ccy == "USD") {
                return round($amount / $currency->sale,2) ;
            }
        }
    }
}
